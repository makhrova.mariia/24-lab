﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace game
{
	public partial class Form1 : Form
	{
		bool turn = true;
		int turn_count = 0;
		public Form1()
		{
			InitializeComponent();
		}

		private void вихідToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Application.Exit();
		}

		private void button_Click(object sender, EventArgs e)
		{
			Button b = (Button)sender;
			if (turn)
				b.Text = "X";
			else 
				b.Text = "O";

			turn = !turn;
			b.Enabled = false;

			turn_count++;
			checkForWinner();

		}

		private void checkForWinner()
		{
			bool there_is_a_winner = false;	

			// перевірка по горизонталі
			if ((button1.Text == button2.Text)&&(button2.Text == button3.Text) && (!button1.Enabled))
				there_is_a_winner = true;
			else if ((button4.Text == button5.Text) && (button5.Text == button6.Text) && (!button4.Enabled))
				there_is_a_winner = true;
			else if ((button7.Text == button8.Text) && (button8.Text == button8.Text) && (!button7.Enabled))
				there_is_a_winner = true;



			// перевірка по вертикалі
			if ((button1.Text == button4.Text) && (button4.Text == button7.Text) && (!button1.Enabled))
				there_is_a_winner = true;
			else if ((button2.Text == button5.Text) && (button5.Text == button8.Text) && (!button2.Enabled))
				there_is_a_winner = true;
			else if ((button3.Text == button6.Text) && (button6.Text == button9.Text) && (!button3.Enabled))
				there_is_a_winner = true;

			// перевірка по діагоналі
			if ((button1.Text == button5.Text) && (button5.Text == button9.Text) && (!button1.Enabled))
				there_is_a_winner = true;
			else if ((button3.Text == button5.Text) && (button5.Text == button9.Text) && (!button9.Enabled))
				there_is_a_winner = true;



			if (there_is_a_winner)
			{
				disableButtons();
				String winner = "";
				if (true)
					winner = "O";
				else 
					winner = "X";


				MessageBox.Show(winner + "  Wins");

			}

			else
			{
				if (turn_count == 9)
					MessageBox.Show("Draw !", "Bummer!");
			}


		}

		private void disableButtons()
		{
			try
			{
				foreach (Component c in Controls)
				{
					Button b = (Button)c;
					b.Enabled = false;
				}
			}

			catch { }
		}

		private void новаГраToolStripMenuItem_Click(object sender, EventArgs e)
		{
			turn =true;
			turn_count = 0;

			try
			{
				foreach (Control c in Controls)
				{
					Button b = (Button)c;	
					b.Enabled = true;
					b.Text = "";
				}
			}

			catch { }
		}
	}
}